﻿using System;
using System.ComponentModel;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace cs481_hw5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        // Initiate all positions >> Go to InitPins() to view related pins
        private readonly Position _myCaliPosition = new Position(33.200139, -117.349995);
        private readonly Position _mySwedishPosition = new Position(59.338859, 17.941122);
        private readonly Position _myFrenchPosition = new Position(47.562350, 7.526584);
        private readonly Position _mySwissPosition = new Position(46.136019, 7.625792);
        private readonly Position _myAustriaPosition = new Position(48.202297, 16.381427
        );
        public MainPage()
        {
            InitializeComponent();
            PinPicker.SelectedIndex = 0;
            var initialPos = MapSpan.FromCenterAndRadius(_myCaliPosition, Distance.FromMeters(200));
            MyMap.MoveToRegion(initialPos);
            MyMap.MapType = MapType.Street;
            InitPins();
        }

        private void PinPicker_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            MapSpan newPos;
            var distance = Distance.FromMeters(50);
            switch (PinPicker.SelectedIndex)
            {
                case 0:
                    newPos = MapSpan.FromCenterAndRadius(_myCaliPosition, distance);
                    break;
                case 1:
                    newPos = MapSpan.FromCenterAndRadius(_mySwedishPosition, distance);
                    break;
                case 2:
                    newPos = MapSpan.FromCenterAndRadius(_myFrenchPosition, distance);
                    break;
                case 3:
                    newPos = MapSpan.FromCenterAndRadius(_mySwissPosition, distance);
                    break;
                case 4:
                    newPos = MapSpan.FromCenterAndRadius(_myAustriaPosition, distance);
                    break;
                default:
                    newPos = MapSpan.FromCenterAndRadius(_myCaliPosition, distance);
                    break;
            }
            MyMap.MoveToRegion(newPos);
        }
        // Initiate all pins and load in the constructor
        private void InitPins()
        {
            var caliPin = new Pin
            {
                Type = PinType.Place,
                Position = _myCaliPosition,
                Label = "Oceanside Castle",
                Address = "My roommate house"
            };
            var swedishPin = new Pin
            {
                Type = PinType.Place,
                Position = _mySwedishPosition,
                Label = "After corona",
                Address = "I'm doing this assignment here"
            };
            var frenchPin = new Pin
            {
                Type = PinType.Place,
                Position = _myFrenchPosition,
                Label = "After Sweden",
                Address = "I go here to code and play in a small room during corona time"
            };
            var swissPin = new Pin
            {
                Type = PinType.Place,
                Position = _mySwissPosition,
                Label = "Ski resort",
                Address = "I want to go here in the mountain to my grand parents"
            };
            var austriaPin = new Pin
            {
                Type = PinType.Place,
                Position = _myAustriaPosition,
                Label = "My dads place",
                Address = "He is not here anymore, somewhere in hidden in the mountain"
            };

            MyMap.Pins.Add(caliPin);
            MyMap.Pins.Add(swedishPin);
            MyMap.Pins.Add(frenchPin);
            MyMap.Pins.Add(swissPin);
            MyMap.Pins.Add(austriaPin);
        }
        // On method for all button, this read the Text of each button
        private void MapType_OnClicked(object sender, EventArgs e)
        {
            var button = (Button)sender;

            switch (button.Text)
            {
                case "Hybrid":
                    MyMap.MapType = MapType.Hybrid;
                    break;
                case "Satellite":
                    MyMap.MapType = MapType.Satellite;
                    break;
                case "Street":
                    MyMap.MapType = MapType.Street;
                    break;
            }
        }
    }
}
